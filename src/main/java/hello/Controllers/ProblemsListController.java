package hello.Controllers;

import hello.Entities.MillenniumPrizeProblem;
import hello.Entities.ProblemsRepository;
import org.apache.naming.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
public class ProblemsListController {
    @Autowired
    private ProblemsRepository problemsRepo;

    @GetMapping("/list")
    public ModelAndView problemsList(HttpSession httpSession, Model model) {
        int size = problemsRepo.findAll().size();
        model.addAttribute("problems", size);

        model.addAttribute("allProblems", problemsRepo.findAll());

        String username = (String)httpSession.getAttribute("username");
        if (username == null) {
            return new ModelAndView("redirect:/");
        }

        return new ModelAndView("main");
    }
}
