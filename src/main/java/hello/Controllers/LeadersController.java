package hello.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class LeadersController
{
    @GetMapping("/leaders")
    public ModelAndView problem(HttpSession httpSession)
    {
        String username = (String) httpSession.getAttribute("username");
        if (username == null)
        {
            return new ModelAndView("redirect:/");
        }

        return new ModelAndView("leaders");
    }
}
