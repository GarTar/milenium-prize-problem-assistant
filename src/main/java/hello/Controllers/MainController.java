package hello.Controllers;

import hello.Entities.MillenniumPrizeProblem;
import hello.Entities.ProblemsRepository;
import hello.Entities.User;
import hello.Entities.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class MainController {
    @Autowired
    private UsersRepository users;

    @Autowired
    private ProblemsRepository problems;
    private MillenniumPrizeProblem mp;

    @GetMapping("/")
    public ModelAndView login(Model model, HttpSession httpSession) {
        String username = (String)httpSession.getAttribute("username");
        if (username != null)
            return new ModelAndView("redirect:/list" );


        model.addAttribute("loginuser", new User());
        return new ModelAndView("index");
    }

    @PostMapping("/")
    public ModelAndView loginPost(@ModelAttribute User user, HttpSession httpSession) {
        if (!users.existsByName(user.getName())) {
            users.save(user);
        }

        httpSession.setAttribute("username", user.getName());
        return new ModelAndView("redirect:list" );
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpSession httpSession) {
        httpSession.removeAttribute("username");

        return new ModelAndView("redirect:/" );
    }

    @RequestMapping("/main")
    public ModelAndView mpMain(HttpSession httpSession, Model model) {
        model.addAttribute("allProblems", problems.findAll());

        String username = (String)httpSession.getAttribute("username");
        if (username == null) {
            return new ModelAndView("redirect:/");
        }



        return new ModelAndView("main");
    }

    @GetMapping("/view_problem/{id}")
    public ModelAndView view_problem(@PathVariable Long id, HttpSession httpSession, Model model) {
        String username = (String)httpSession.getAttribute("username");
        if (username == null)
            return new ModelAndView("redirect:/" );
        
        mp = problems.findById(id).get();
        model.addAttribute("mp", mp);
        return new ModelAndView("problem_view");
    }

    @PostMapping("/view_problem/{id}")
    public ModelAndView view_problem_post(@PathVariable Long pId, HttpSession httpSession, Model model) {
        String username = (String)httpSession.getAttribute("username");
        if (username == null)
        {
            return new ModelAndView("redirect:/");
        }

        mp = problems.findById(pId).get();
        model.addAttribute("mp", mp);
        return new ModelAndView("problem_view");
    }
}