package hello.Controllers;

import hello.Entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.*;

@Controller
public class ProblemViewController {
    @Autowired
    private ProblemsRepository problems;

    @Autowired
    private UsersRepository users;

    @Autowired
    private UserNotesRepository userNotes;

    private MotivationRepository motivationRepository = new HaradcodedRandomMotivationRepository();

    @GetMapping("/problem/{id}")
    public ModelAndView problem(@PathVariable("id") long problemId, HttpSession httpSession, Model model) {
        String username = (String)httpSession.getAttribute("username");
        if (username == null)
            return new ModelAndView("redirect:/" );


        MillenniumPrizeProblem mp = problems.findById(problemId).get();
        model.addAttribute("mp", mp);
        model.addAttribute("message", motivationRepository.getMotivationMessage());
        return new ModelAndView("problem_view");
    }

    @PostMapping("/problem/{id}")
    public ModelAndView postComment(@PathVariable("id") long problemId, @ModelAttribute UserNote note, HttpSession httpSession, Model model, @RequestParam("pic") MultipartFile file)
    {
        String username = (String)httpSession.getAttribute("username");
        if (username == null)
            return new ModelAndView("redirect:/" );


       /* User user = users.findAll().stream().filter(u -> Objects.equals(u.getName(), username)).findFirst().get();

        user.addUserNote(note);
        users.save(user);*/

        MillenniumPrizeProblem mp = problems.findById(problemId).get();

        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[20];
        random.nextBytes(bytes);

        MessageDigest md = null;
        String rand = null;
        String name = null;
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] arr = md.digest(bytes);
            rand = Base64.getEncoder().encodeToString(arr);
            name =  rand.substring(0, 8);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String ext = "";
        String fileName = file.getOriginalFilename();

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            ext = fileName.substring(i+1);
        }

        if (!file.getOriginalFilename().isEmpty()) {
            String realPath = null;
            URL r = this.getClass().getResource("/");
            System.out.println(r.getPath());

            File dir = new File(r.getPath() + "/resources");
            // if the directory does not exist, create it
            if (!dir.exists()) {
                try {
                    dir.mkdir();
                }
                catch(SecurityException se) {
                }
            }

            File transferFile = new File(r.getPath() + "/resources/" + name + "."+ext);
            try {
                file.transferTo(transferFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        note.setDate(Calendar.getInstance().getTime());
        note.setAuthor(username);
        if (!file.getOriginalFilename().isEmpty())
            note.setPicture_uri("/resources/"+ name + "."+ext);
        userNotes.save(note);
        mp.addUserNote(note);
        problems.save(mp);

        model.addAttribute("mp", mp);
        model.addAttribute("message", motivationRepository.getMotivationMessage());

        System.out.println(file.getOriginalFilename());
        return new ModelAndView("problem_view");
    }
}
