package hello;

import hello.Entities.MillenniumPrizeProblem;
import hello.Entities.ProblemsRepository;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.sql.*;
import java.util.ArrayList;

@SpringBootApplication
public class Application
{
    public static void main(String[] args) {
        try {
            SpringApplication.run(Application.class, args); //NOSONAR
        } catch (BeanCreationException ex) {
            Throwable realCause = unwrap(ex);
            ex.printStackTrace();
        }
    }

    public static Throwable unwrap(Throwable ex) {
        if (ex != null && BeanCreationException.class.isAssignableFrom(ex.getClass())) {
            return unwrap(ex.getCause());
        } else {
            return ex;
        }
    }
}
