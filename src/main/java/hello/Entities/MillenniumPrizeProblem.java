package hello.Entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Problems")
public class MillenniumPrizeProblem {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String name;

    @OneToMany
    private List<UserNote> userNotes;

    @Lob
    @Column(name="description", length=10240)
    private String description;

    protected MillenniumPrizeProblem() {}

    public MillenniumPrizeProblem(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format(
                "MillenniumPrizeProblem[id=%d, firstName='%s', lastName='%s']",
                id, name, description);
    }

    public void addUserNote(UserNote note) {
        userNotes.add(note);
    }

    public List<UserNote> getUserNotes() {
        return userNotes;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}