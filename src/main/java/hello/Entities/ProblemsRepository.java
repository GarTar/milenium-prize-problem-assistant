package hello.Entities;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ProblemsRepository extends CrudRepository<MillenniumPrizeProblem, Long> {

    List<MillenniumPrizeProblem> findAll();
}