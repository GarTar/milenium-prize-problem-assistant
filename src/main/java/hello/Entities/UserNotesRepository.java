package hello.Entities;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface UserNotesRepository extends CrudRepository<UserNote, Long> {

    List<UserNote> findAll();
}