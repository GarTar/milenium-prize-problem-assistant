package hello.Entities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
public class DbSeeder {
    @Autowired
    private ProblemsRepository repository;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        repository.save(new MillenniumPrizeProblem("P versus NP", "The question is whether or not, for all problems for which an algorithm can verify a given solution quickly (that is, in polynomial time), an algorithm can also find that solution quickly. Since the former describes the class of problems termed NP, while the latter describes P, the question is equivalent to asking whether all problems in NP are also in P. This is generally considered one of the most important open questions in mathematics and theoretical computer science as it has far-reaching consequences to other problems in mathematics, and to biology, philosophy[5] and cryptography (see P versus NP problem proof consequences). A common example of an NP problem not known to be in P is the Boolean satisfiability problem.\n" +
                "\n" +
                "Most mathematicians and computer scientists expect that P ≠ NP; however, it remains to be proven.[6]\n" +
                "\n" +
                "The official statement of the problem was given by Stephen Cook."));

        repository.save(new MillenniumPrizeProblem("Hodge conjecture", "The Hodge conjecture is that for projective algebraic varieties, Hodge cycles are rational linear combinations of algebraic cycles.\n" +
                "\n" +
                "The official statement of the problem was given by Pierre Deligne."));
        repository.save(new MillenniumPrizeProblem("Riemann hypothesis", "The Riemann hypothesis is that all nontrivial zeros of the analytical continuation of the Riemann zeta function have a real part of 1/2. A proof or disproof of this would have far-reaching implications in number theory, especially for the distribution of prime numbers. This was Hilbert's eighth problem, and is still considered an important open problem a century later.\n" +
                "\n" +
                "The official statement of the problem was given by Enrico Bombieri."));
        repository.save(new MillenniumPrizeProblem("Yang–Mills existence and mass gap", "In physics, classical Yang–Mills theory is a generalization of the Maxwell theory of electromagnetism where the chromo-electromagnetic field itself carries charges. As a classical field theory it has solutions which travel at the speed of light so that its quantum version should describe massless particles (gluons). However, the postulated phenomenon of color confinement permits only bound states of gluons, forming massive particles. This is the mass gap. Another aspect of confinement is asymptotic freedom which makes it conceivable that quantum Yang-Mills theory exists without restriction to low energy scales. The problem is to establish rigorously the existence of the quantum Yang–Mills theory and a mass gap.\n" +
                "\n" +
                "The official statement of the problem was given by Arthur Jaffe and Edward Witten.[7]"));
        repository.save(new MillenniumPrizeProblem("Navier–Stokes existence and smoothness", "The Navier–Stokes equations describe the motion of fluids, and are one of the pillars of fluid mechanics. However, theoretical understanding of their solutions is incomplete. In particular, solutions of the Navier–Stokes equations often include turbulence, for which general solutions remains one of the greatest unsolved problems in physics, despite its immense importance in science and engineering.\n" +
                "\n" +
                "Even basic properties of the solutions to Navier–Stokes have never been proven. For the three-dimensional system of equations, and given some initial conditions, mathematicians have not yet proved that smooth solutions always exist, or that if they do exist, they have bounded energy per unit mass.[citation needed] This is called the Navier–Stokes existence and smoothness problem.\n" +
                "\n" +
                "The problem is to make progress towards a mathematical theory that will give insight into these equations, by proving either that smooth, globally defined solutions exist that meet certain conditions, or that they do not always exist and the equations break down.\n" +
                "\n" +
                "The official statement of the problem was given by Charles Fefferman."));
        repository.save(new MillenniumPrizeProblem("Birch and Swinnerton-Dyer conjecture", "The Birch and Swinnerton-Dyer conjecture deals with certain types of equations: those defining elliptic curves over the rational numbers. The conjecture is that there is a simple way to tell whether such equations have a finite or infinite number of rational solutions. Hilbert's tenth problem dealt with a more general type of equation, and in that case it was proven that there is no way to decide whether a given equation even has any solutions.\n" +
                "\n" +
                "The official statement of the problem was given by Andrew Wiles.[8]"));
    }
}
