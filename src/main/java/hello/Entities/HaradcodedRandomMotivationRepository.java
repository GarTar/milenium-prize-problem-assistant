package hello.Entities;

import java.util.Random;

public class HaradcodedRandomMotivationRepository implements MotivationRepository
{
    private String[] messages = new String[]{
            "If you fall asleep now, you will dream. If you study now, you will live your dream.",
            "When you think it’s too late, the truth is, it’s still early.",
            "The pain of studying is only temporary. But the pain of not knowing – ignorance — is forever.",
            "Studying is not about time. It’s about effort.",
            "Life is not all about studying. But if you can’t even conquer this little part of life, then what else can you possibly do?",
            "Enjoy the inexorable pain.",
            "It’s those who are earlier than the others, those who put in more effort, who can enjoy the feelings of success.",
            "Not everyone can truly succeed in everything. But success only comes with self-management and determination.",
            "Time is flying.",
            "The saliva that flow now will become the tears of tomorrow.",
            "If you don’t walk today, you’ll have to run tomorrow.",
            "People who invest in the future are realists.",
            "The level of education is in direct correlation with your salary.",
            "When today is over, it will never come back.",
            "Even now, your enemies are eagerly flipping through books.",
            "No pain, no gain."};
    
    private Random random = new Random();

    @Override
    public String getMotivationMessage()
    {
        return messages[random.nextInt(messages.length)];
    }
}
