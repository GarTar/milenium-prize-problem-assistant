package hello.Entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String name;

    public User() {}

    public User(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format(
                "UserNote[id=%d, text='%s']",
                id, name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }
}