package hello.Entities;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsersRepository extends CrudRepository<User, Long> {

    List<User> findAll();

    boolean existsByName(String name);
}