package hello.Entities;

public interface MotivationRepository
{
    String getMotivationMessage();
}
