package hello.Entities;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "UserNotes")
public class UserNote {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String text;
    private String author;
    private Date date;
    private String picture_uri;

    protected UserNote() {}

    public UserNote(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return String.format(
                "UserNote[id=%d, text='%s']",
                id, text);
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Long getId() {
        return id;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public Date getDate()
    {
        return (Date) date.clone();
    }

    public void setDate(Date date)
    {
        this.date = (Date) date.clone();
    }

    public String getPicture_uri() { return picture_uri; }

    public void setPicture_uri(String picture_uri) { this.picture_uri = picture_uri; }
}